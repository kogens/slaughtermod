# Slaughtermod for BF2

## Description
An old project of mine, a fun little mod for Battlefield 2 that replaces guns with lasers, puts guns all over vehicles and turn teddy bears into C4. From the old [ModDB site](https://www.moddb.com/mods/slaughter1/):
>A simple minimod for bf2. Think violent weapons, lots of explosions, teddybears for c4, huge lasers, tanks shooting artillery shells, miniguns for co-gunners, handheld .50 cal, G3 with HE rounds and so on.


## Screenshots
![Lazor](docs/images/screen249.jpg)
![Gunbuggy](docs/images/screen176.jpg)
![Blodmedic](docs/images/screen222.jpg)
![Manned tank](docs/images/37667.jpg)
![Magic missiles](docs/images/screen312.jpg)

And here's a little userbar for your webforum signature!

![slaughtermod userbar](docs/images/userbarslaughteruserwc2.png)
